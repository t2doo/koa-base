const path = require('path')
const Koa = require('koa')
const parameter = require('koa-parameter')
const KoaBody = require('koa-body')
const KoaStatic = require('koa-static')
const cors = require('@koa/cors')
const errHandler = require('./errHandler')

const router = require('../router')
const app = new Koa()
app.use(parameter(app))
app.use(KoaBody({
  multipart: true,
  formidable: {
    uploadDir: path.join(__dirname, '../upload'),
    keepExtensions: true
  },
  parsedMethods: ['POST', 'PUT', 'PATCH', 'DELETE']
}))
app.use(KoaStatic(path.join(__dirname, '../upload')))
app.use(cors())
app.use(router.routes()).use(router.allowedMethods())

app.on('error', errHandler)

module.exports = app
