const Router = require('koa-router')
const {
  userValidator,
  verifyUser,
  bcryptPassword,
  verifyLogin,
  isEmail
} = require('../middleware/user.middleware')
const { auth } = require('../middleware/auth.middleware')
const { register, login, changePassword, getLoginUser } = require('../controller/user.controller')

const router = new Router({ prefix: '/users' })

router.post('/register', userValidator, isEmail, verifyUser, bcryptPassword, register)
router.post('/login', userValidator, verifyLogin, login)
router.patch('/change', auth, userValidator, bcryptPassword, changePassword)
router.get('/', auth, getLoginUser)

module.exports = router
