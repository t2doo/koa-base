const jwt = require('jsonwebtoken')
const { tokenExpireError, invalidToken } = require('../constant/err.type')
const { JWT_SECRET } = require('../config/default')

const auth = async (ctx, next) => {
  // 取出 authorization
  const { authorization } = ctx.request.header
  // 将取出的 authorization 去掉拼接的 `Bearer ` 请求头
  const token = authorization.replace('Bearer ', '')

  try {
    // 验证成功, 挂载 user
    ctx.state.user = jwt.verify(token, JWT_SECRET)
  } catch (err) {
    switch (err.name) {
      case 'TokenExpireError':
        console.error('token已过期', err)
        return ctx.app.emit('error', tokenExpireError, ctx)
      case 'JsonWebTokenError':
        console.error('无效的token', err)
        return ctx.app.emit('error', invalidToken, ctx)
    }
  }
  await next()
}

module.exports = {
  auth
}
