const setErrorType = (code, message, result = '') => {
  return {
    code,
    message,
    result
  }
}

module.exports = {
  userFormatError: setErrorType('10001', '用户名或密码为空'),
  userExists: setErrorType('10002', '用户已存在'),
  userRegisterError: setErrorType('10003', '用户注册错误'),
  userNotExists: setErrorType('10004', '用户不存在'),
  userLoginError: setErrorType('10005', '用户登录失败'),
  invalidPassword: setErrorType('10006', '密码不匹配'),
  changePasswordError: setErrorType('10007', '修改密码失败'),
  invalidEmail: setErrorType('10008', '邮箱格式不正确'),
  tokenExpireError: setErrorType('10101', 'token已过期'),
  invalidToken: setErrorType('10102', '无效的token')
}
