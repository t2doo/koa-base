const { Sequelize } = require('sequelize')

const {
  MYSQL_HOST,
  MYSQL_PORT,
  MYSQL_USER,
  MYSQL_PWD,
  MYSQL_DB
} = require('../config/default')

const db = new Sequelize(
  MYSQL_DB,
  MYSQL_USER,
  MYSQL_PWD,
  {
    host: MYSQL_HOST,
    port: MYSQL_PORT,
    dialect: 'mysql'
  }
)

// testConnection(db)

module.exports = db

function testConnection (dbModel) {
  dbModel.authenticate().then(() => {
    console.log('数据库连接成功')
  }).catch (err => {
    console.log('数据库连接失败', err)
  })
}
