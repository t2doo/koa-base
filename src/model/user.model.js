const { DataTypes } = require('sequelize')
const db = require('../db')
const { setAdditionalType } = require('./base.model')

const User = db.define('user', {
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    comment: '用户名， 唯一'
  },
  password: {
    type: DataTypes.CHAR(64),
    allowNull: false,
    comment: '密码'
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '邮箱'
  },
  email_verified_at: {
    type: DataTypes.DATE,
    allowNull: true,
    // defaultValue: db.literal('CURRENT_TIMESTAMP'),
    comment: '邮箱认证'
  },
  remember_token: {
    type: DataTypes.CHAR(64),
    allowNull: true
  }
}, setAdditionalType)

// User.sync({ force: true })

module.exports = User
