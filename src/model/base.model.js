const setAdditionalType = {
    paranoid: true,
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at'
  }

module.exports = {
  setAdditionalType
}
